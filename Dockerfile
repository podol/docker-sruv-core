FROM alpine:latest
COPY sruv.jar /var/lib/sruv/
RUN adduser -h /var/lib/sruv -u 1666 sruser sruser -g "SRU User" -s /sbin/nologin -D \
 && apk add --no-cache openjdk8
EXPOSE 8080
